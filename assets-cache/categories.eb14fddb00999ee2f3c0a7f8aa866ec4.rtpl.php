<?php if(!class_exists('Rain\Tpl')){exit;}?>  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Categories</h1>
      <a href="addCategory" class="btn-action">Add new Category</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Code</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>
      <?php $counter1=-1;  if( isset($categories) && ( is_array($categories) || $categories instanceof Traversable ) && sizeof($categories) ) foreach( $categories as $key1 => $value1 ){ $counter1++; ?>
        <tr class="data-row">
              <option value="<?php echo htmlspecialchars( $value1["id"], ENT_COMPAT, 'UTF-8', FALSE ); ?>"></option>
            <td class="data-grid-td">
               <span class="data-grid-cell-content"><?php echo htmlspecialchars( $value1["name"], ENT_COMPAT, 'UTF-8', FALSE ); ?></span>
            </td>
            <td class="data-grid-td">
               <span class="data-grid-cell-content"><?php echo htmlspecialchars( $value1["code"], ENT_COMPAT, 'UTF-8', FALSE ); ?></span>
            </td>
        
        
            <td class="data-grid-td">
              <div class="actions">
                <div class="action edit"><span><a href="/editCategory/<?php echo htmlspecialchars( $value1["id"], ENT_COMPAT, 'UTF-8', FALSE ); ?>">Edit</a></span></div>
                <div class="action delete"><span><a href="/deleteCategory/<?php echo htmlspecialchars( $value1["id"], ENT_COMPAT, 'UTF-8', FALSE ); ?>">Delete</a></span></div>
              </div>
            </td>
        </tr>
      <?php } ?>
    </table>
  </main>
  <!-- Main Content -->