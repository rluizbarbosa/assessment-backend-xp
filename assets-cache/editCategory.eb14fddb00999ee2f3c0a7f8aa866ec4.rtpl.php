<?php if(!class_exists('Rain\Tpl')){exit;}?>  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Edit Category</h1>
    
    <form method="post" action="/editCategory">
      <div class="input-field">
        <label for="category-name" class="label">Category Name</label>
        <input type="text" name="name" value="<?php echo htmlspecialchars( $category["name"], ENT_COMPAT, 'UTF-8', FALSE ); ?>" id="category-name" class="input-text" />
        
      </div>
      <div class="input-field">
        <label for="category-code" class="label">Category Code</label>
        <input type="text" name="code" value="<?php echo htmlspecialchars( $category["code"], ENT_COMPAT, 'UTF-8', FALSE ); ?>" id="category-code" class="input-text" />
      </div>
      <input type="hidden" name="id" id="category-id" value="<?php echo htmlspecialchars( $category["id"], ENT_COMPAT, 'UTF-8', FALSE ); ?>" />
      <div class="actions-form">
        <a href="/categories" class="action back">Back</a>
        <input class="btn-submit btn-action"  type="submit" value="Save" />
      </div>
    </form>
  </main>
  <!-- Main Content -->