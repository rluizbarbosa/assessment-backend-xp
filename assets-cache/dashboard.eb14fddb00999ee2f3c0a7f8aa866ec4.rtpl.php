<?php if(!class_exists('Rain\Tpl')){exit;}?>  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Dashboard</h1>
    </div>
    <div class="infor">
      You have <?php echo htmlspecialchars( $qtd, ENT_COMPAT, 'UTF-8', FALSE ); ?> products added on this store: <a href="addProduct" class="btn-action">Add new Product</a>
    </div>
    <ul class="product-list">
      <?php $counter1=-1;  if( isset($products) && ( is_array($products) || $products instanceof Traversable ) && sizeof($products) ) foreach( $products as $key1 => $value1 ){ $counter1++; ?>
      <li>
        <div class="product-image">
          <a href="/editProduct/<?php echo htmlspecialchars( $value1["id"], ENT_COMPAT, 'UTF-8', FALSE ); ?>" title="<?php echo htmlspecialchars( $value1["name"], ENT_COMPAT, 'UTF-8', FALSE ); ?>">
            <img src="assets/images/product/<?php echo htmlspecialchars( $value1["id"], ENT_COMPAT, 'UTF-8', FALSE ); ?>/<?php echo htmlspecialchars( $value1["image"], ENT_COMPAT, 'UTF-8', FALSE ); ?>" layout="responsive" width="164" height="145" alt="Tênis Runner Bolt" />
          </a>
        </div>
        <div class="product-info">
          <div class="product-name"><span><?php echo htmlspecialchars( $value1["name"], ENT_COMPAT, 'UTF-8', FALSE ); ?></span></div>
          <div class="product-price"><span class="special-price">9 available</span> <span>R$<?php echo htmlspecialchars( $value1["price"], ENT_COMPAT, 'UTF-8', FALSE ); ?></span></div>
        </div>
      </li>
      <?php } ?>
    </ul>
  </main>
  <!-- Main Content -->
