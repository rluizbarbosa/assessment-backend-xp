<?php 

namespace Teste;

use Rain\Tpl;
use \Facebook\Facebook;
use Teste\Model\Product;
use \Teste\Model\Category;

class Page {


	private $tpl;
	private $options = [];
	private $defaults = [
		"data"=>[]
	];

	public function __construct($opts = array(), $tpl_dir = "/assets/")
	{

		$this->defaults["data"]["session"] = $_SESSION;

		$this->options = array_merge($this->defaults, $opts);

		$config = array(
		    "base_url"      => null,
		    "tpl_dir"       => $_SERVER['DOCUMENT_ROOT'].$tpl_dir,
		    "cache_dir"     => $_SERVER['DOCUMENT_ROOT']."/assets-cache/",
		    "debug"         => false
		);

		Tpl::configure( $config );

		$this->tpl = new Tpl();

		if ($this->options['data']) $this->setData($this->options['data']);
		
		$this->setTpl("header", array("data" => $this->options['data']));

	}


	public function __destruct()
	{

		$this->tpl->draw("footer", false);

	}

	private function setData($data = array())
	{

		foreach($data as $key => $val)
		{

			$this->tpl->assign($key, $val);

		}

	}
	
	public function setTpl($tplname, $data = array(), $returnHTML = false)
	{

		$this->setData($data);

		return $this->tpl->draw($tplname, $returnHTML);

	}
}

 ?>