<?php 

namespace Teste\Model;

use \Teste\Model;
use \Teste\DB\Sql;
use \Teste\Model\Product;

class Category extends Model {

	protected $fields = [
		"name", "code"
	];

	public static function listCategories($search = array())
	{

		$sql = new Sql();

		$categories = $sql->select("SELECT * FROM category ORDER BY insert_data DESC");

		return $categories;

	}

	public function save(){
		$sql = new Sql();

		$sql->query("INSERT INTO category (name, code, insert_data, update_data) VALUES (:name, :code, :insert_data, :update_data)", array(
			":name"=>$this->getname(),
			":code"=>$this->getcode(),
			":insert_data"=>time(),
			":update_data"=>time()
			));
	}

	public function delete($id)
	{
		$sql = new Sql();

		$sql->query("DELETE FROM category WHERE id = :id", array(
			":id"=>$id
			));
	}

	public function getCategory($id)
	{
		$sql = new Sql();

		$category = $sql->select("SELECT * FROM category WHERE id = :id LIMIT 1", array(
			":id"=>$id
		));

		return $category[0];

	}

	public function update()
	{
		$sql = new Sql();

		$sql->query("UPDATE category SET name = :name, code = :code, update_data = :update_data WHERE id = :id", array(
			":name"=>$this->getname(),
			":code"=>$this->getcode(),
			":update_data"=>time(),
			":id"=>$this->getid()
		));
	}

}?>