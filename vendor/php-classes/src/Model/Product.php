<?php 

namespace Teste\Model;

use \Teste\Model;
use \Teste\DB\Sql;
use \Teste\Model\Category;
use \Intervention\Image;

class Product extends Model {

	protected $fields = [
		"id", "name", "sku", "price", "quantity", "description", "image", "categories"
	];

	public static function listProducts($search = array())
	{

		$sql = new Sql();

		$products = $sql->select("SELECT * FROM product ORDER BY insert_data DESC");

		for ($i=0; $i < count($products); $i++) { 
			$products[$i]['id'] = $products[$i]['id'];
			$products[$i]['name'] = $products[$i]['name'];
			$products[$i]['sku'] = $products[$i]['sku'];
			$products[$i]['price'] = number_format($products[$i]['price'], '2', ',', '.');
			$products[$i]['description'] = $products[$i]['description'];
			$products[$i]['insert_data'] = date("H:i:s d/m/Y" , $products[$i]['insert_data']);
			$products[$i]['update_data'] = date("H:i:s d/m/Y" , $products[$i]['update_data']);

			$categories = $sql->select("SELECT c.name FROM categories cs LEFT JOIN category c ON cs.category = c.id WHERE cs.product = :product", array(
				':product' => $products[$i]['id']
			));

			$products[$i]['categories'] = $categories;
		}

		return $products;

	}

	public function save()
	{
		$sql = new Sql();

		$sql->query("INSERT INTO product (name, sku, price, quantity, description, insert_data, update_data) VALUES (:name, :sku, :price, :quantity, :description, :insert_data, :update_data)", array(
			":name"=>$this->getname(),
			":sku"=>$this->getsku(),
			":price"=>number_format($this->getprice(), 2, ".", ""),
			":quantity"=>(int)$this->getquantity(),
			":description"=>$this->getdescription(),
			":insert_data"=>time(),
			":update_data"=>time()
			));

		$product_id = $sql->select("SELECT LAST_INSERT_ID()");

		foreach ($this->getcategory() as $key => $value) {
			$sql->query("INSERT INTO categories (product, category) VALUES (:product, :category)", array(
			":product"=>(int)$product_id[0]['LAST_INSERT_ID()'],
			":category"=>(int)$value,
			));
		}

		$this->setid((int)$product_id[0]['LAST_INSERT_ID()']);

		return $this->getid();
	}

	public function delete($id)
	{
		$sql = new Sql();

		$product = $sql->select("SELECT image FROM product WHERE id = :id LIMIT 1", array(
			":id"=>$id
		));

		if ($product[0]['image'] !== null && $product[0]['image'] !== "") {
			unlink($_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . 
				"assets". DIRECTORY_SEPARATOR . 
				"images". DIRECTORY_SEPARATOR . 
				"product". DIRECTORY_SEPARATOR .
				$id . DIRECTORY_SEPARATOR . 
				$product[0]['image']);
			rmdir($_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . 
				"assets". DIRECTORY_SEPARATOR . 
				"images". DIRECTORY_SEPARATOR . 
				"product". DIRECTORY_SEPARATOR .
				$id);
		}

		$sql->query("DELETE FROM product WHERE id = :id", array(
			":id"=>$id
			));
	}

	public function getProduct($id)
	{
		$sql = new Sql();

		$product = $sql->select("SELECT * FROM product WHERE id = :id LIMIT 1", array(
			":id"=>$id
		));

		$categories = new Category();
		$cats = $categories->listCategories();

		$categories = $sql->select("SELECT c.id as id_category FROM categories cs LEFT JOIN category c ON cs.category = c.id WHERE cs.product = :product", array(
				':product' => $product[0]['id']
			));

		for ($i=0; $i < count($cats); $i++) { 
			$product[0]['categories'][$i] = $cats[$i];
			for ($t=0; $t < count($categories); $t++) { 
				if ($categories[$t]['id_category'] === $cats[$i]['id']) {
					$product[0]['categories'][$i]['contem'] = true;
					break;
				}else{
					$product[0]['categories'][$i]['contem'] = false;
				}
			}
		}

		return $product[0];

	}

	public function update()
	{
		$sql = new Sql();

		$sql->query("UPDATE product SET name = :name, sku = :sku, price = :price, quantity = :quantity, description = :description, update_data = :update_data WHERE id = :id", array(
			":name"=>$this->getname(),
			":sku"=>$this->getsku(),
			":price"=>number_format($this->getprice(), 2, ".", ""),
			":quantity"=>(int)$this->getquantity(),
			":description"=>$this->getdescription(),
			":update_data"=>time(),
			":id"=>$this->getid()
		));

		$sql->query("DELETE FROM categories WHERE product = :product", array(
			":product"=>(int)$this->getid()
			));

		foreach ($this->getcategory() as $key => $value) {
			$sql->query("INSERT INTO categories (product, category) VALUES (:product, :category)", array(
			":product"=>(int)$this->getid(),
			":category"=>(int)$value,
			));
		}
	}

	public function setNewImage($product_id,  $file)
	{
		$nome = 'product_'.md5(uniqid(rand(), true));

		$extension = explode('.' , $file['name']);
		$extension = end($extension);

		switch($extension){

			case "jpg":
			case "jpeg":
				$image = imagecreatefromjpeg($file["tmp_name"]);
			break;

			case 'gif':
				$image = imagecreatefromgif($file["tmp_name"]);
			break;
			
			case 'png':
				$image = imagecreatefrompng($file["tmp_name"]);
			break;

			default:
				$image =  "semImg";
			break;

		}

		if($image == "semImg"){
			return false;
		}else{

			$sql = new Sql();
			
			$product = $sql->select("SELECT image FROM product WHERE id = :id LIMIT 1", array(
				":id"=>$product_id
			));

			if ($product[0]['image'] !== null && $product[0]['image'] !== "") {
				unlink($_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . 
					"assets". DIRECTORY_SEPARATOR . 
					"images". DIRECTORY_SEPARATOR . 
					"product". DIRECTORY_SEPARATOR .
					$product_id . DIRECTORY_SEPARATOR . 
					$product[0]['image']);
			}

			$x = imagesx($image);
			$y = imagesy($image);
			
			$largura = ($x > 1300) ? 1300 : $x;
			$altura = ($largura*$y) / $x;
			
			if($altura>1300){
				$altura = 1300;
				$largura = ($altura*$x) / $y;
			}
			
			$imageFim = imagecreatetruecolor($largura, $altura);

			imagecopyresampled($imageFim, $image, 0, 0, 0, 0, $largura, $altura, $x, $y);

			if (!is_dir($_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "assets" . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "product" . DIRECTORY_SEPARATOR . $product_id )) {
				mkdir(	$_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "assets" . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "product" . DIRECTORY_SEPARATOR . $product_id);
			}

			$dist = $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . 
			"assets". DIRECTORY_SEPARATOR . 
			"images". DIRECTORY_SEPARATOR . 
			"product". DIRECTORY_SEPARATOR .
			$product_id . DIRECTORY_SEPARATOR . 
			$nome . ".jpg";

			imagejpeg($imageFim, $dist);

			imagedestroy($image);
			imagedestroy($imageFim);

			$sql->query("UPDATE product SET image = :image WHERE id = :id", array(
				':id' => $product_id,
				':image' => $nome.".jpg"
			));

			return $dist;
		}
		
	}

}?>