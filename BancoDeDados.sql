-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Tempo de geração: 31-Jul-2020 às 20:06
-- Versão do servidor: 10.4.10-MariaDB
-- versão do PHP: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `webjump`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_product_has_category_product` (`product`),
  KEY `fk_product_has_category_product_c` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categories`
--

INSERT INTO `categories` (`id`, `product`, `category`) VALUES
(77, 23, 8),
(78, 24, 9),
(79, 24, 8),
(87, 28, 8),
(88, 28, 6),
(89, 29, 8),
(90, 30, 9);

-- --------------------------------------------------------

--
-- Estrutura da tabela `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `insert_data` int(11) NOT NULL,
  `update_data` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `category`
--

INSERT INTO `category` (`id`, `name`, `code`, `insert_data`, `update_data`) VALUES
(6, 'Brinco', 'BRC', 1596219598, 1596219598),
(8, 'Sapatenis', 'SPT', 1596219743, 1596219743),
(9, 'Blusa', 'BLS', 1596219752, 1596219968);

-- --------------------------------------------------------

--
-- Estrutura da tabela `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `quantity` int(11) DEFAULT 0,
  `description` longtext DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `insert_data` int(11) NOT NULL,
  `update_data` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `product`
--

INSERT INTO `product` (`id`, `name`, `sku`, `price`, `quantity`, `description`, `image`, `insert_data`, `update_data`) VALUES
(23, 'Tenis 1', 'TN1', '59.00', 10, 'Descrição de teste', 'product_9e8160de421cc816ea310de5b165a416.jpg', 1596224912, 1596224912),
(24, 'Tenis 2', 'TN2', '79.00', 25, 'Descrição', 'product_58c9f88c0e12f7695b37dee1c7975d48.jpg', 1596224938, 1596224938),
(28, 'Tenis 3', 'TN3', '56.00', 24, 'Descrição', 'product_151cbfe758aebeddce5fd25b666d3cd9.jpg', 1596225447, 1596225447),
(29, 'Tenis 4', 'TN4', '24.00', 32, 'Descrição', 'product_b4d48aa7612ea6f4f51b40b174f8b64b.jpg', 1596225472, 1596225472),
(30, 'Tenis 5 ', 'TN5', '41.99', 24, 'descrição', 'product_e0eef1d9cb895432252c126761d717af.jpg', 1596225904, 1596225904);

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `fk_product_has_category_product` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_product_has_category_product_c` FOREIGN KEY (`category`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
