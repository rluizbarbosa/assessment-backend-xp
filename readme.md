# Desafio para avaliar conhecimento
Este desafio foi realizado por Rodrigo Luiz Barbosa de Souza com o intuito de avaliação dos conhecimentos para a vaga de Backend na WebJump.

# Instruções para rodar o projeto
- Faça um clone desse projeto no diretório raiz em sua maquina local
- Crie um banco de dados chamado webjump e rode o arquivo BancoDeDados.sql
- Abra seu navegador em localhost e seja feliz