<?php 
date_default_timezone_set('America/Sao_Paulo');
session_start();
require_once("vendor/autoload.php");
error_reporting(E_ALL ^ E_NOTICE);

use \Slim\App;
use \Teste\Page;
use \Teste\DB\Sql;
use \Teste\Model\Product;
use \Teste\Model\Category;

$app = new App();

//Primeira pagina do site
$app->get('/', function ($request, $response, $args) {
	$products = new Product();
	$page = new Page(['data' => array('page' => 'Dashboard')], "/assets/");
	$listProducts = $products->listProducts();
	$page->setTpl("dashboard", array(
		'products' => $listProducts,
		'qtd' => count($listProducts)
	));
});

//
$app->get('/categories', function ($request, $response, $args) {
	$categories = new Category();
	$page = new Page(['data' => array('page' => 'Categories')], "/assets/");
	$page->setTpl("categories", array(
		'categories' => $categories->listCategories()
	));
});

//
$app->get('/addCategory', function ($request, $response, $args) {
	$page = new Page(['data' => array('page' => 'New Category')], "/assets/");
	$page->setTpl("addCategory", []);
});

//
$app->post('/addCategory', function ($request, $response, $args) {
	$category = new Category();
	$category->setData($_POST);
	$category->save();
	header("Location: /categories");
	exit();
});

//
$app->get('/editCategory/{id}', function ($request, $response, $args) {
	$category = new Category();
	$page = new Page(['data' => array('page' => 'Edit Category')], "/assets/");
	$page->setTpl("editCategory", array(
		'category' => $category->getCategory($args['id'])
	));
});

$app->post('/editCategory', function ($request, $response, $args) {
	$category = new Category();
	$category->setData($_POST);
	$category->update();
	header("Location: /categories");
	exit();
});

//
$app->get('/deleteCategory/{id}', function ($request, $response, $args) {
	$category = new Category();
	$category->delete($args['id']);
	header("Location: /categories");
	exit();
});

//
$app->get('/addProduct', function ($request, $response, $args) {
	$categories = new Category();
	$page = new Page(['data' => array('page' => 'New Product')], "/assets/");
	$page->setTpl("addProduct", array(
		'categories' => $categories->listCategories()
	));
});

$app->post('/addProduct', function ($request, $response, $args) {
	$products = new Product();
	$products->setData($_POST);
	$product_id = $products->save();
	$products->setNewImage($product_id, $_FILES['image']);
	header("Location: /products");
	exit();
});

$app->get('/products', function ($request, $response, $args) {
	$products = new Product();
	$page = new Page(['data' => array('page' => 'Products')], "/assets/");
	$page->setTpl("products", array(
		'products' => $products->listProducts()
	));
});

//
$app->get('/editProduct/{id}', function ($request, $response, $args) {
	$products = new Product();
	$page = new Page(['data' => array('page' => 'Edit Product')], "/assets/");
	$page->setTpl("editProduct", array(
		'product' => $products->getProduct($args['id'])
	));
});

$app->post('/editProduct', function ($request, $response, $args) {
	$products = new Product();
	$products->setData($_POST);
	$products->update();
	$products->setNewImage($_POST['id'], $_FILES['image']);
	header("Location: /products");
	exit();
});
//
$app->get('/deleteProduct/{id}', function ($request, $response, $args) {
	$products = new Product();
	$products->delete($args['id']);
	header("Location: /products");
	exit();
});

//
$app->get('/{product}', function ($request, $response, $args) {
	var_dump($args['product']);
	exit();
});
$app->run();?>